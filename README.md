# kotlin-99-problems

99 problems, converted into Kotlin.

For the most part this is a clone of https://github.com/dkandalov/kotlin-99, but with the implementations removed (replaced with TODO()), and using Junit 5.

Please see: https://github.com/dkandalov/kotlin-99/blob/master/readme.md for instructions.


Don't feel you need to stick to the functions provided, change them as you see fit, but you may also have to change the tests if you do.

