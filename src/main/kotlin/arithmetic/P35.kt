package arithmetic

import com.natpryce.hamkrest.assertion.assertThat
import com.natpryce.hamkrest.equalTo
import org.junit.jupiter.api.Test

fun Int.primeFactors(): List<Int> {
   TODO()
}

class P35Test {
    @Test fun `determine prime factors of a given positive integer`() {
        assertThat((2..10).map { Pair(it, it.primeFactors()) }, equalTo(listOf(
            Pair(2, listOf(2)),
            Pair(3, listOf(3)),
            Pair(4, listOf(2, 2)),
            Pair(5, listOf(5)),
            Pair(6, listOf(2, 3)),
            Pair(7, listOf(7)),
            Pair(8, listOf(2, 2, 2)),
            Pair(9, listOf(3, 3)),
            Pair(10, listOf(2, 5))
        )))
        assertThat(315.primeFactors(), equalTo(listOf(3, 3, 5, 7)))
    }
}
