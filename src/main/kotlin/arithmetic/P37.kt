package arithmetic

import com.natpryce.hamkrest.assertion.assertThat
import com.natpryce.hamkrest.equalTo
import org.junit.jupiter.api.Test
import common.tail

fun Int.totient2(): Int { TODO() }

class P37Test {
    @Test fun `calculate Euler's totient function (improved)`() {
        assertThat(10.totient2(), equalTo(4))
        assertThat(10090.totient2(), equalTo(4032))
    }
}
