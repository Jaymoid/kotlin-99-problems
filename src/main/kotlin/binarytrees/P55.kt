package binarytrees

import com.natpryce.hamkrest.assertion.assertThat
import org.junit.jupiter.api.Test
import binarytrees.Tree.End
import binarytrees.Tree.Node
import common.containsAll

fun <T> balancedTrees(treeSize: Int, value: T): List<Tree<T>> { TODO() }

class P55Test {
    @Test fun `construct all balanced trees`() {
        assertThat(balancedTrees(1, "x"), containsAll(nodeList(Node("x"))))
        assertThat(balancedTrees(2, "x"), containsAll(nodeList(
            Node("x", End, Node("x")),
            Node("x", Node("x"), End)
        )))
        assertThat(balancedTrees(3, "x"), containsAll(nodeList(
            Node("x", Node("x"), Node("x"))
        )))
        assertThat(balancedTrees(4, "x"), containsAll(nodeList(
            Node("x", Node("x"), Node("x", End, Node("x"))),
            Node("x", Node("x", End, Node("x")), Node("x")),
            Node("x", Node("x"), Node("x", Node("x"), End)),
            Node("x", Node("x", Node("x"), End), Node("x"))
        )))
    }

    companion object {
        fun nodeList(vararg nodes: Node<String>): Iterable<Tree<String>> = nodes.asList()
    }
}
