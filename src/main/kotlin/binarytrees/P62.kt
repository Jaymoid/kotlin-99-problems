package binarytrees

import com.natpryce.hamkrest.assertion.assertThat
import org.junit.jupiter.api.Test
import binarytrees.Tree.End
import binarytrees.Tree.Node
import common.containsAll


fun <T> Tree<T>.valuesAtLevel(n: Int): List<T> { TODO() }


class P62Test {
    @Test fun `collect node values at particular level`() {
        val tree = Node("a", Node("b"), Node("c", Node("d"), Node("e")))
        assertThat(tree.valuesAtLevel(0), containsAll(emptyList()))
        assertThat(tree.valuesAtLevel(1), containsAll("a"))
        assertThat(tree.valuesAtLevel(2), containsAll("b", "c"))
        assertThat(tree.valuesAtLevel(3), containsAll("d", "e"))
        assertThat(tree.valuesAtLevel(4), containsAll(emptyList()))
    }
}
