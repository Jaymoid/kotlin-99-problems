package binarytrees

import com.natpryce.hamkrest.Matcher
import com.natpryce.hamkrest.and
import com.natpryce.hamkrest.anyElement
import com.natpryce.hamkrest.assertion.assertThat
import com.natpryce.hamkrest.equalTo
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test
import binarytrees.P55Test.Companion.nodeList
import binarytrees.Tree.End
import binarytrees.Tree.Node
import common.containsAll
import common.tail

fun <T> heightBalancedTrees(height: Int, value: T): List<Tree<T>> { TODO() }

fun Tree<*>.height(): Int{ TODO() }

fun <T> Tree<T>.nodes(): List<Node<T>> { TODO() }

class P59Test {
    @Test fun `construct all height-balanced binary trees`() {
        assertThat(heightBalancedTrees(1, "x"), containsAll(nodeList(Node("x"))))
        assertThat(heightBalancedTrees(2, "x"), containsAll(nodeList(
            Node("x", Node("x"), Node("x")),
            Node("x", End, Node("x")),
            Node("x", Node("x"), End)
        )))

        assertThat(heightBalancedTrees(3, "x"), containsElements<Tree<String>>(
            equalTo(
                Node("x",
                     Node("x", End, Node("x")),
                     Node("x", Node("x"), End))),
            equalTo(
                Node("x",
                     Node("x", Node("x"), End),
                     Node("x", End, Node("x"))))
        ))

        heightBalancedTrees(3, "x").flatMap { it.nodes() }.forEach { node ->
            assertTrue(node.left.height() - node.right.height() <= 1)
        }
    }

    private fun <T> containsElements(vararg matchers: Matcher<T>): Matcher<Iterable<T>> {
        return matchers.tail().fold(anyElement(matchers.first())) { result: Matcher<Iterable<T>>, matcher ->
            result.and(anyElement(matcher))
        }
    }
}
