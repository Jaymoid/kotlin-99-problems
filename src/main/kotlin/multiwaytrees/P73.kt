package multiwaytrees

import com.natpryce.hamkrest.assertion.assertThat
import com.natpryce.hamkrest.equalTo
import org.junit.jupiter.api.Test
import common.tail
import java.util.*

fun MTree<*>.toLispString(): String {
    TODO()
}

fun String.fromLispString(): MTree<String> {
    TODO()
}


private object SExprParser: TokenParser {
    override fun parse(s: String): Token? {
        TODO()
    }
}

private fun Token.toMTree(): MTree<String>{
    TODO()
}

private interface Token {
    fun length(): Int
    fun trim(): Token?
}

private data class Text(val value: String): Token {
    override fun trim():Token?  {
        TODO()
    }
    override fun length(): Int{
        TODO()
    }
    override fun toString() : String{
        TODO()
    }
}

private data class Atom(val value: String): Token {
    override fun trim():Token?  {
        TODO()
    }
    override fun length(): Int{
        TODO()
    }
    override fun toString() : String{
        TODO()
    }
}

private data class Seq(val tokens: List<Token>): Token {
    constructor (vararg tokens: Token) : this(tokens.asList())

    override fun trim(): Token? {
        TODO()
    }

    override fun length(): Int {
        TODO()
    }

    override fun toString(): String {
        TODO()
    }
}


private interface TokenParser {
    fun parse(s: String): Token?
}

private class TextParser(val value: String): TokenParser {
    override fun parse(s: String): Token?{
        TODO()
    }
}

private val LParenParser = TextParser("(")
private val RParenParser = TextParser(")")
private val SpaceParser = TextParser(" ")

private object AtomParser: TokenParser {
    override fun parse(s: String): Token? {
        TODO()
    }
}

private class SequenceParser(val tokenParsers: List<TokenParser>): TokenParser {
    constructor(vararg tokenParsers: TokenParser): this(tokenParsers.asList())

    override fun parse(s: String): Token? {
        TODO()
    }
}

private class RepeatedParser(val tokenParser: TokenParser): TokenParser {
    private fun Seq.prepend(token: Token): Seq = Seq(listOf(token) + tokens)

    override fun parse(s: String): Seq {
        TODO()
    }
}

private class OrParser(vararg val tokenParsers: TokenParser): TokenParser {
    override fun parse(s: String): Token? {
        TODO()
    }
}


class P73Test {
    @Test fun `convert multiway tree to lisp string`() {
        assertThat(MTree("a").toLispString(), equalTo("a"))
        assertThat(MTree("a", MTree("b"), MTree("c")).toLispString(), equalTo("(a b c)"))
        assertThat(MTree("a", MTree("b"), MTree("c", MTree("d"))).toLispString(), equalTo("(a b (c d))"))
        assertThat("afg^^c^bd^e^^^".convertToMTree().toLispString(), equalTo("(a (f g) c (b d e))"))
    }

    @Test fun `parse s-expression into tokens`() {
        assertThat(SExprParser.parse("a")!!.trim()!!, equalTo<Token>(Atom("a")))
        assertThat(SExprParser.parse("(a)")!!.trim()!!, equalTo<Token>(Atom("a")))
        assertThat(SExprParser.parse("(a b c d)")!!.trim()!!, equalTo<Token>(
            Seq(Atom("a"),
                Seq(Atom("b"), Atom("c"), Atom("d")))
        ))
        assertThat(SExprParser.parse("(a b (c d))")!!.trim()!!, equalTo<Token>(
            Seq(Atom("a"),
                Seq(Atom("b"),
                    Seq(Atom("c"), Atom("d"))
                )
            )
        ))
    }

    @Test fun `transform tokens into multiway tree`() {
        assertThat(
            Seq(Atom("a"),
                Seq(Atom("b"), Atom("c"), Atom("d"))
            ).toMTree(),
            equalTo(MTree("a", MTree("b"), MTree("c"), MTree("d"))))

        assertThat(
            Seq(Atom("a"),
                Seq(Atom("b"), Atom("c"), Atom("d"))
            ).toMTree(),
            equalTo(MTree("a", MTree("b"), MTree("c"), MTree("d"))))
    }

    @Test fun `convert lisp string to multiway tree`() {
        assertThat("a".fromLispString(), equalTo(MTree("a")))
        assertThat("(a b c)".fromLispString(), equalTo(MTree("a", MTree("b"), MTree("c"))))
        assertThat("(a b (c d))".fromLispString(), equalTo(MTree("a", MTree("b"), MTree("c", MTree("d")))))
        assertThat("(a (f g) c (b d e))".fromLispString(), equalTo(
            MTree("a",
                  MTree("f", MTree("g")),
                  MTree("c"),
                  MTree("b", MTree("d"), MTree("e"))
            )
        ))
    }
}
