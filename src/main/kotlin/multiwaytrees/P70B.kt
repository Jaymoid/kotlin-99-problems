package multiwaytrees

import com.natpryce.hamkrest.assertion.assertThat
import com.natpryce.hamkrest.equalTo
import org.junit.jupiter.api.Test
import java.util.*

/**
 * <tree> ::= <value> { <tree> } "^"
 */
fun String.convertToMTree(): MTree<Char> {
    TODO()
}

private fun String.convertToMTree(position: Int): Pair<MTree<Char>, Int> {
    TODO()
}

fun MTree<Char>.convertToString(): String {
    TODO()
}


class P70BTest {
    @Test fun `construct multiway tree from string`() {
        assertThat("a^".convertToMTree(), equalTo(MTree('a')))
        assertThat("ab^c^^".convertToMTree(), equalTo(MTree('a', MTree('b'), MTree('c'))))
        assertThat("afg^^c^bd^e^^^".convertToMTree(), equalTo(
            MTree('a',
                  MTree('f', MTree('g')),
                  MTree('c'),
                  MTree('b', MTree('d'), MTree('e'))
            )
        ))
    }

    @Test fun `convert multiway tree to string`() {
        assertThat(MTree('a').convertToString(), equalTo("a^"))
        assertThat(MTree('a', MTree('b'), MTree('c')).convertToString(), equalTo("ab^c^^"))
        assertThat(
            MTree('a',
                  MTree('f', MTree('g')),
                  MTree('c'),
                  MTree('b', MTree('d'), MTree('e'))
            ).convertToString(),
            equalTo("afg^^c^bd^e^^^")
        )
    }
}
