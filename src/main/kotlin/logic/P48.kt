package logic

import com.natpryce.hamkrest.assertion.assertThat
import com.natpryce.hamkrest.equalTo
import org.junit.jupiter.api.Test

// Functions below cannot be "inline" because there are recursive.

fun Boolean.and_(vararg others: Boolean): Boolean {
    TODO()
}

fun Boolean.or_(vararg others: Boolean): Boolean {
    TODO()
}

fun Boolean.xor_(vararg others: Boolean): Boolean {
    TODO()
}

private fun BooleanArray.tail() = { TODO() }


class P48Test {
    @Test
    fun `logical expressions`() {
        assertThat(true.and_(true, false, true), equalTo(false))
        assertThat(true.or_(true, false, false), equalTo(true))
        assertThat(true.xor_(true, false, true), equalTo(true))
    }
}
