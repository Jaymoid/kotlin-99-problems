package lists

import com.natpryce.hamkrest.assertion.assertThat
import com.natpryce.hamkrest.equalTo
import org.junit.jupiter.api.Assertions.assertNull
import org.junit.jupiter.api.Test

fun <T> penultimate(list: List<T>): T = TODO()

class P02Test {

    @Test
    fun `find the last but one element of a list`() {
        assertThat(penultimate(listOf(1, 1, 2, 3, 5, 8)), equalTo(5))
    }

    @Test
    fun `penultimate element in list smaller than 2 returns null`() {
        assertNull(penultimate(listOf(1)))
    }
}
