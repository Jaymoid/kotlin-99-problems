package lists

import com.natpryce.hamkrest.assertion.assertThat
import com.natpryce.hamkrest.equalTo
import org.junit.jupiter.api.Assertions.assertNull
import org.junit.jupiter.api.Test

fun last(list: List<Int>): Int? {
    TODO()
}

class P01Test {
    @Test
    fun `find the last element of a list`() {
        assertThat(last(listOf(1, 1, 2, 3, 5, 8)), equalTo(8))
    }

    @Test
    fun `last index in empty list returns Null`() {
        assertNull(last(listOf<Int>()))
    }
}