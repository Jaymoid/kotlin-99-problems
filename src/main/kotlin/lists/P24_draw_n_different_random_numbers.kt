package lists

import com.natpryce.hamkrest.assertion.assertThat
import com.natpryce.hamkrest.equalTo
import org.junit.jupiter.api.Test
import common.containsAll

import java.util.*

fun lotto(n: Int, k: Int, random: Random = Random()): List<Int> = TODO()

class P24Test {
    @Test
    fun `draw N different random numbers from the set 1 to M`() {
        assertThat(lotto(3, 49, Random(123)), equalTo(listOf(32, 28, 8)))
    }
}
