package misc

import com.natpryce.hamkrest.anyElement
import com.natpryce.hamkrest.assertion.assertThat
import com.natpryce.hamkrest.equalTo
import common.containsAll
import graphs.Graph
import graphs.toGraph
import org.junit.jupiter.api.Test

fun <V> Graph<V, *>.gracefulLabeling(): Sequence<Graph<String, Nothing>> {
    TODO()
}


class P92Test {
    @Test
    fun `basic graceful labeling`() {
        assertThat("[a]".toGraph().gracefulLabeling().first(), equalTo("[1]".toGraph()))

        assertThat("[a-b]".toGraph().gracefulLabeling().toList(), containsAll("[1-2]".toGraph(), "[2-1]".toGraph()))

        assertThat(
            "[a-b, a-c]".toGraph().gracefulLabeling().toList(), containsAll(
                "[1-2, 1-3]".toGraph(), "[1-3, 1-2]".toGraph(),
                "[3-1, 3-2]".toGraph(), "[3-2, 3-1]".toGraph()
            )
        )
    }

    @Test
    fun `graceful labeling of examples in readme`() {
        assertThat(
            "[a-d, a-g, a-b, b-c, b-e, e-f]".toGraph().gracefulLabeling().toList(), anyElement(
                equalTo(
                    "[7-2, 7-1, 7-3, 3-6, 3-5, 5-4]".toGraph()
                )
            )
        )

        // TODO too slow
//        assertThat("[a-i, a-h, a-g, a-b, a-c, c-f, c-d, d-k, c-e, e-g, g-m, g-n, n-p]".toGraph().gracefulLabeling().first(), equalTo(
//                "[7-2, 7-1, 7-3, 3-6, 3-5, 5-4]".toGraph()
//        ))
    }
}
