package misc

import com.natpryce.hamkrest.assertion.assertThat
import com.natpryce.hamkrest.equalTo
import org.junit.jupiter.api.Test
import misc.EightQueens.Companion.Board
import misc.EightQueens.Companion.Queen
import misc.EightQueens.Companion.solutions

@Suppress("unused")
class EightQueens {
    companion object {
        fun solutions(boardSize: Int): List<Board> {
            TODO()
        }


        fun solutions(board: Board): List<Board> {
            TODO()
        }

        data class Queen(val row: Int, val column: Int)

        data class Board(val size: Int, val queens: List<Queen>) {

            constructor(vararg queens: Queen) : this(maxPosition(queens), queens.asList())

            fun nextMoves(): List<Board> {
                TODO()
            }


            fun isComplete(): Boolean {
                TODO()
            }


            private fun isValidMove(queen: Queen): Boolean {
                TODO()
            }


            fun toPrettyString(): String {
                TODO()
            }


            companion object {
                private fun maxPosition(queens: Array<out Queen>): Int {
                    TODO()
                }

            }
        }
    }
}


class P90Test {
    @Test
    fun `solutions for eight queen puzzle`() {
        assertThat(solutions(boardSize = 0), equalTo(listOf(Board())))
        assertThat(solutions(boardSize = 1), equalTo(listOf(Board(Queen(0, 0)))))
        assertThat(solutions(boardSize = 2), equalTo(emptyList()))
        assertThat(solutions(boardSize = 3), equalTo(emptyList()))

        assertThat(
            solutions(boardSize = 4).map(Board::toPrettyString), equalTo(listOf(
                """
            |--*-
            |*---
            |---*
            |-*--
            """,
                """
            |-*--
            |---*
            |*---
            |--*-
            """
            ).map { it.trimMargin() })
        )

        assertThat(solutions(boardSize = 8).size, equalTo(92))
    }

    @Test
    fun `convert board to string`() {
        assertThat(
            Board(Queen(0, 0), Queen(2, 1), Queen(1, 2)).toPrettyString(), equalTo(
                """
            |*--
            |--*
            |-*-
        """.trimMargin()
            )
        )
    }
}
