package graphs

import com.natpryce.hamkrest.assertion.assertThat
import com.natpryce.hamkrest.equalTo
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test
import common.containsAll
import common.tail
import graphs.Graph.*
import graphs.Graph.TermForm.Term
import graphs.P80Test.Companion.equivalentTo
import java.util.*

fun <V, L> Graph<V, L>.spanningTrees(): List<Graph<V, L>>{
    TODO()
}


fun Graph<*, *>.isTree(): Boolean {
    TODO()
}


fun Graph<*, *>.isConnected(): Boolean {
    TODO()
}



class P83Test {
    @Test fun `find all spanning trees`() {
        assertThat("[a]".toGraph().spanningTrees(), containsAll(listOf("[a]".toGraph())))
        assertThat("[a-b]".toGraph().spanningTrees(), containsAll(listOf("[a-b]".toGraph())))

        assertThat("[a-b, b-c, c-a]".toGraph().spanningTrees(), containsAll(listOf(
            "[a-b, b-c]".toGraph(),
            "[a-b, c-a]".toGraph(),
            "[b-c, c-a]".toGraph()
        )) { equivalentTo(it) })

        "[a-b, b-c, b-d, b-e, a-f]".toGraph().let {
            assertThat(it.spanningTrees(), containsAll(listOf(it)) { equivalentTo(it) })
            assertThat(it.isTree(), equalTo(true))
        }
    }

    @Test fun `no spanning trees for disjoint graph`() {
        "[a-b, c-d]".toGraph().let {
            assertThat(it.spanningTrees(), equalTo(emptyList()))
            assertThat(it.isConnected(), equalTo(false))
        }
    }

    @Test fun `all spanning trees of graph from illustration`() {
        val graph = "[a-b, a-d, b-c, b-e, c-e, d-e, d-f, d-g, e-h, f-g, g-h]".toGraph()
        val spanningTrees = graph.spanningTrees()
        println(spanningTrees)

        assertTrue(spanningTrees.any { it.equivalentTo("[d-f, a-d, a-b, b-c, b-e, d-g, e-h]".toGraph()) })
        assertThat(spanningTrees.size, equalTo(112))
    }
}
